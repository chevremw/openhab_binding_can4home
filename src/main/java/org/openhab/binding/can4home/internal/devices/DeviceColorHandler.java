/**
 * Copyright (c) 2010-2019 Contributors to the openHAB project
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.openhab.binding.can4home.internal.devices;

import static org.openhab.binding.can4home.internal.CAN4HomeConstants.*;

import java.io.IOException;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.smarthome.core.library.types.HSBType;
import org.eclipse.smarthome.core.thing.Bridge;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingStatus;
import org.eclipse.smarthome.core.thing.ThingStatusDetail;
import org.eclipse.smarthome.core.types.Command;
import org.eclipse.smarthome.core.types.RefreshType;
import org.openhab.binding.can4home.internal.bridges.CAN4HomeProtocol;
import org.openhab.binding.can4home.internal.bridges.CAN4HomeProtocol.CANPacket;
import org.openhab.binding.can4home.internal.bridges.CAN4HomeProtocol.PacketException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The {@link CAN4HomeHandler} is responsible for handling commands, which are
 * sent to one of the channels.
 *
 * @author William Chèvremont - Initial contribution
 */
@NonNullByDefault
public class DeviceColorHandler extends CANDevice {
    private final Logger logger = LoggerFactory.getLogger(DeviceColorHandler.class);

    public DeviceColorHandler(Thing thing) {
        super(thing);
    }

    // Commands from openHAB
    @SuppressWarnings("null")
    @Override
    public void handleCommand(ChannelUID channelUID, Command command) {
        if (command == RefreshType.REFRESH) {

        } else if (HSBType.class.isInstance(command)) {
            try {
                Bridge B = getBridge();
                if (B != null && CAN4HomeProtocol.class.isInstance(B.getHandler())) {
                    int sRGB = ((HSBType) command).getRGB();

                    CAN4HomeProtocol bdg = (CAN4HomeProtocol) B.getHandler();
                    CANPacket pkt = getDataPacket(4);
                    pkt.command = CANPacket.CMD_WRITE;
                    pkt.data[0] = (byte) ((sRGB >> 24) & 0x000000FF);
                    pkt.data[1] = (byte) ((sRGB >> 16) & 0x000000FF);
                    pkt.data[2] = (byte) ((sRGB >> 8) & 0x000000FF);
                    pkt.data[3] = (byte) (sRGB & 0x000000FF);
                    bdg.send(pkt);
                }
            } catch (IOException | PacketException e) {
                logger.error(e.toString());
            }
        }
    }

    // Commands from CAN bus
    @Override
    public void receive(CANPacket p) {
        if (p.command == CANPacket.CMD_CHANINFO) {
            byte type = p.data[0];
            if ((type & TYPES_MASK) == TYPE_COLOR) {
                updateStatus(ThingStatus.ONLINE);
            } else {
                updateStatus(ThingStatus.OFFLINE, ThingStatusDetail.CONFIGURATION_ERROR);
            }
        } else {
            logger.error("CAN command " + String.format("0x%02x", p.command) + " unimplemented");
        }
    }

    @Override
    public void initialize() {
        updateStatus(ThingStatus.UNKNOWN);
        super.initialize();
    }
}
