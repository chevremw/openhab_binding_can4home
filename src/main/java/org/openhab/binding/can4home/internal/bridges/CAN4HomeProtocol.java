/**
 * Copyright (c) 2010-2019 Contributors to the openHAB project
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.openhab.binding.can4home.internal.bridges;

import java.io.IOException;

import org.eclipse.jdt.annotation.NonNullByDefault;

/**
 * @author William Chèvremont - Initial contribution
 */
@NonNullByDefault
public interface CAN4HomeProtocol {

    public class CANPacket {

        public static final byte CMD_DATA = 0x00;
        public static final byte CMD_WRITE = 0x01;
        public static final byte CMD_CHANINFO = 0x02;
        public static final byte CMD_SUBSCRIBE = 0x03;

        public byte groupAddress;
        public byte nodeAddress;
        public byte command;
        public byte cmdOpt;
        public boolean isRequest;
        public byte dataLength;
        public byte[] data = new byte[0];

        @Override
        public String toString() {
            String s = String.format("%02x in %02x :: ", nodeAddress, groupAddress);
            s += String.format("%02x %02x", command, cmdOpt);
            if (isRequest) {
                s += " request " + Integer.toString(dataLength) + " bytes";
            } else {
                s += " data: ";
                for (int i = 0; i < data.length; i++) {
                    s += String.format("%02x ", data[i]);
                }
            }
            return s;
        }

        public Integer toHash() {
            Integer hash = (int) cmdOpt;
            hash += nodeAddress << 8;
            hash += groupAddress << 16;
            return hash;
        }
    }

    public class PacketException extends Exception {
        private static final long serialVersionUID = 8906762798848758617L;

        public PacketException(String msg) {
            super(msg);
        }
    }

    public abstract void send(CANPacket p) throws IOException;
}
