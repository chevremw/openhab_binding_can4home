/**
 * Copyright (c) 2010-2019 Contributors to the openHAB project
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.openhab.binding.can4home.internal.devices;

import static org.openhab.binding.can4home.internal.CAN4HomeConstants.*;

import java.io.IOException;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.smarthome.core.thing.Bridge;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingStatus;
import org.eclipse.smarthome.core.thing.ThingStatusDetail;
import org.eclipse.smarthome.core.types.Command;
import org.eclipse.smarthome.core.types.RefreshType;
import org.openhab.binding.can4home.internal.bridges.CAN4HomeProtocol;
import org.openhab.binding.can4home.internal.bridges.CAN4HomeProtocol.CANPacket;
import org.openhab.binding.can4home.internal.bridges.CAN4HomeProtocol.PacketException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The {@link CAN4HomeHandler} is responsible for handling commands, which are
 * sent to one of the channels.
 *
 * @author William Chèvremont - Initial contribution
 */
@NonNullByDefault
public class DeviceContactHandler extends CANDevice {
    private final Logger logger = LoggerFactory.getLogger(DeviceContactHandler.class);

    public DeviceContactHandler(Thing thing) {
        super(thing);
    }

    // Commands from openHAB
    @SuppressWarnings("null")
    @Override
    public void handleCommand(ChannelUID channelUID, Command command) {
        if (command == RefreshType.REFRESH) {
            try {
                CANPacket pkt = getRequestPacket(1);
                Bridge B = getBridge();
                if (B != null && CAN4HomeProtocol.class.isInstance(B.getHandler())) {
                    CAN4HomeProtocol bdg = (CAN4HomeProtocol) B.getHandler();
                    bdg.send(pkt);
                }
            } catch (PacketException | IOException e) {
                logger.error(e.getMessage());
            }
        }
    }

    // Commands from CAN bus
    @Override
    public void receive(CANPacket p) throws PacketException {
        checkPacket(p);

        if (p.command == CANPacket.CMD_DATA) {

            if (p.isRequest) {
                return;
                // throw new PacketException("Request not supported");
            }

            if (p.data.length != 1) {
                throw new PacketException("Data must be one uint8");
            }

            if (p.data[0] == (byte) 1) {
                triggerChannel(CONTACT_CH_EVENT, CONTACT_PRESSED);
            } else if (p.data[0] == (byte) 2) {
                triggerChannel(CONTACT_CH_EVENT, CONTACT_RELEASED);
            } else if (p.data[0] == (byte) 3) {
                triggerChannel(CONTACT_CH_EVENT, CONTACT_RELEASED_LG);
            } else {
                logger.error("Command " + p.data[0] + " not recognized");
            }
        } else if (p.command == CANPacket.CMD_CHANINFO) {
            byte type = p.data[0];
            if ((type & TYPES_MASK) == TYPE_CONTACT) {
                updateStatus(ThingStatus.ONLINE);
            } else {
                updateStatus(ThingStatus.OFFLINE, ThingStatusDetail.CONFIGURATION_ERROR);
            }
        } else {
            logger.error("CAN command " + String.format("0x%02x", p.command) + " unimplemented");
        }
    }

    @Override
    public void initialize() {
        updateStatus(ThingStatus.UNKNOWN);
        super.initialize();
        // updateStatus(ThingStatus.ONLINE); // Emit chantype request and go online only on response.
    }
}
