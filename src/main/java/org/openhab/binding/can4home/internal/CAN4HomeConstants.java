/**
 * Copyright (c) 2010-2019 Contributors to the openHAB project
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.openhab.binding.can4home.internal;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.smarthome.core.thing.ThingTypeUID;

/**
 *
 * @author William Chèvremont - Initial contribution
 */

@NonNullByDefault
public final class CAN4HomeConstants {

    public static final String BINDING_ID = "can4home";

    public static final ThingTypeUID THING_CAN4HOME_MCP2515_BRIDGE = new ThingTypeUID(BINDING_ID, "mcp2515");
    public static final ThingTypeUID THING_CAN4HOME_CONTACT = new ThingTypeUID(BINDING_ID, "contact");
    public static final ThingTypeUID THING_CAN4HOME_SWITCH = new ThingTypeUID(BINDING_ID, "switch");
    public static final ThingTypeUID THING_CAN4HOME_COLOR = new ThingTypeUID(BINDING_ID, "color");
    public static final ThingTypeUID THING_CAN4HOME_DIMMER = new ThingTypeUID(BINDING_ID, "dimmer");
    public static final ThingTypeUID THING_CAN4HOME_TEMPERATURE = new ThingTypeUID(BINDING_ID, "temperature");
    public static final ThingTypeUID THING_CAN4HOME_RELHUMID = new ThingTypeUID(BINDING_ID, "relHumidity");
    public static final ThingTypeUID THING_CAN4HOME_ROLLER = new ThingTypeUID(BINDING_ID, "roller");
    public static final ThingTypeUID THING_CAN4HOME_RAW = new ThingTypeUID(BINDING_ID, "rawValue");

    public static final byte TYPE_RAW_MASK = (byte) 0x0F;
    public static final byte TYPES_MASK = (byte) 0x3F;

    public static final byte TYPE_WRITE = (byte) 0x80;
    public static final byte TYPE_READ = (byte) 0x40;
    public static final byte TYPE_UINT8 = (byte) 0x01;
    public static final byte TYPE_UINT16 = (byte) 0x02;
    public static final byte TYPE_UINT32 = (byte) 0x03;
    public static final byte TYPE_UINT64 = (byte) 0x04;
    public static final byte TYPE_INT8 = (byte) 0x05;
    public static final byte TYPE_INT16 = (byte) 0x06;
    public static final byte TYPE_INT32 = (byte) 0x07;
    public static final byte TYPE_INT64 = (byte) 0x08;
    public static final byte TYPE_FLOAT32 = (byte) 0x09;
    public static final byte TYPE_FLOAT64 = (byte) 0x0A;

    public static final byte TYPE_SWITCH = (byte) 0x10;
    public static final byte TYPE_CONTACT = (byte) 0x11;
    public static final byte TYPE_COLOR = (byte) 0x12;
    public static final byte TYPE_DIMMER = (byte) 0x13;
    public static final byte TYPE_TEMP = (byte) 0x14;
    public static final byte TYPE_RELHUM = (byte) 0x15;
    public static final byte TYPE_ROLLER = (byte) 0x16;

    // Abstract CAN device
    public static final String GROUP_ADDRESS = "groupAddress";
    public static final String NODE_ADDRESS = "nodeAddress";
    public static final String CHAN_NUMBER = "channelNumber";

    // Contact
    public static final String CONTACT_CH_EVENT = "event";
    public static final String CONTACT_PRESSED = "PRESSED";
    public static final String CONTACT_RELEASED = "RELEASED";
    public static final String CONTACT_RELEASED_LG = "RELEASED_LONG";

    // Bridge MCP2515
    public static final String DEVICE = "SPI_device";
    public static final String SPEED = "SPI_speed";
    public static final String INTPIN = "SPI_INT";

    public static final String MODE = "SPI_mode";
    public static final String MODE_0 = "MODE0";
    public static final String MODE_1 = "MODE1";
    public static final String MODE_2 = "MODE2";
    public static final String MODE_3 = "MODE3";

    public static final String CANSPEED = "CAN_speed";
    public static final String CAN_500kbps = "500kbps";
    public static final String CAN_250kbps = "250kbps";
    public static final String CAN_125kbps = "125kbps";
    public static final String CAN_62p5kbps = "62p5kbps";

}
