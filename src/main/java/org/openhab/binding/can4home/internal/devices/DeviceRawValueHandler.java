/**
 * Copyright (c) 2010-2019 Contributors to the openHAB project
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.openhab.binding.can4home.internal.devices;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.smarthome.core.library.types.DecimalType;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingStatus;
import org.eclipse.smarthome.core.thing.ThingStatusDetail;
import org.eclipse.smarthome.core.types.Command;
import org.openhab.binding.can4home.internal.bridges.CAN4HomeProtocol.CANPacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The {@link CAN4HomeHandler} is responsible for handling commands, which are
 * sent to one of the channels.
 *
 * @author William Chèvremont - Initial contribution
 */
@NonNullByDefault
public class DeviceRawValueHandler extends CANDevice {
    private final Logger logger = LoggerFactory.getLogger(DeviceRawValueHandler.class);

    public enum type {
        UINT8((byte) 1, true),
        UINT16((byte) 2, true),
        UINT32((byte) 4, true),
        UINT64((byte) 8, true),
        INT8((byte) 1, false),
        INT16((byte) 2, false),
        INT32((byte) 4, false),
        INT64((byte) 8, false),
        NONE((byte) 0, false);

        private byte m_length;
        private boolean m_unsign;

        private type(byte length, boolean unsigned) {
            m_length = length;
            m_unsign = unsigned;
        }

        public static type fromMask(byte mask) {
            switch (mask) {
                case 0x01:
                    return UINT8;
                case 0x02:
                    return UINT16;
                case 0x03:
                    return UINT32;
                case 0x04:
                    return UINT64;
                case 0x05:
                    return INT8;
                case 0x06:
                    return INT16;
                case 0x07:
                    return INT32;
                case 0x08:
                    return INT64;
            }
            return NONE;
        }

        public Long getValue(byte[] dat) throws Exception {
            if (this.equals(NONE)) {
                throw new Exception("None type can't get value");
            }

            if (dat.length != m_length) {
                throw new Exception("length do not correspond");
            }
            Long val = new Long(0);
            Long tmp;

            for (int i = 0; i < m_length; i++) {
                tmp = Long.rotateLeft(dat[m_length - i - 1] & 0x00FF, i * 8);
                val += tmp;
            }

            return val;
        }
    }

    private type m_type = type.NONE;

    public DeviceRawValueHandler(Thing thing) {
        super(thing);
    }

    // Commands from openHAB
    @Override
    public void handleCommand(ChannelUID channelUID, Command command) {

    }

    // Commands from CAN bus
    @Override
    public void receive(CANPacket p) {
        if (p.command == CANPacket.CMD_CHANINFO) {
            byte T = p.data[0];
            m_type = type.fromMask((byte) (T & 0x3F));
            if (m_type != type.NONE) {
                updateStatus(ThingStatus.ONLINE);
            } else {
                updateStatus(ThingStatus.OFFLINE, ThingStatusDetail.CONFIGURATION_ERROR);
            }
        } else if (p.command == CANPacket.CMD_DATA) {
            try {
                Long val = m_type.getValue(p.data);
                updateState("value", new DecimalType(val));
            } catch (Exception e) {
                logger.error(e.toString());
            }
        } else {
            logger.error("CAN command " + String.format("0x%02x", p.command) + " unimplemented");
        }
    }

    @Override
    public void initialize() {
        updateStatus(ThingStatus.UNKNOWN);
        super.initialize();
    }
}
