/**
 * Copyright (c) 2010-2019 Contributors to the openHAB project
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.openhab.binding.can4home.internal.devices;

import static org.openhab.binding.can4home.internal.CAN4HomeConstants.*;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingStatus;
import org.eclipse.smarthome.core.thing.ThingStatusDetail;
import org.eclipse.smarthome.core.types.Command;
import org.openhab.binding.can4home.internal.bridges.CAN4HomeProtocol.CANPacket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The {@link CAN4HomeHandler} is responsible for handling commands, which are
 * sent to one of the channels.
 *
 * @author William Chèvremont - Initial contribution
 */
@NonNullByDefault
public class DeviceDimmerHandler extends CANDevice {
    private final Logger logger = LoggerFactory.getLogger(DeviceDimmerHandler.class);

    public DeviceDimmerHandler(Thing thing) {
        super(thing);
    }

    // Commands from openHAB
    @Override
    public void handleCommand(ChannelUID channelUID, Command command) {

    }

    // Commands from CAN bus
    @Override
    public void receive(CANPacket p) {
        if (p.command == CANPacket.CMD_CHANINFO) {
            byte type = p.data[0];
            if ((type & TYPES_MASK) == TYPE_DIMMER) {
                updateStatus(ThingStatus.ONLINE);
            } else {
                updateStatus(ThingStatus.OFFLINE, ThingStatusDetail.CONFIGURATION_ERROR);
            }
        } else {
            logger.error("CAN command " + String.format("0x%02x", p.command) + " unimplemented");
        }
    }

    @Override
    public void initialize() {
        updateStatus(ThingStatus.UNKNOWN);
        super.initialize();
    }
}
