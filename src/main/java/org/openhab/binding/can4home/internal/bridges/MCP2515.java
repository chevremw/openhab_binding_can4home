/**
 * Copyright (c) 2010-2019 Contributors to the openHAB project
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.openhab.binding.can4home.internal.bridges;

import static org.openhab.binding.can4home.internal.CAN4HomeConstants.*;

import java.io.IOException;
import java.util.HashMap;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.smarthome.config.core.Configuration;
import org.eclipse.smarthome.core.thing.Bridge;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingStatus;
import org.eclipse.smarthome.core.thing.binding.BaseBridgeHandler;
import org.eclipse.smarthome.core.thing.binding.ThingHandler;
import org.eclipse.smarthome.core.types.Command;
import org.openhab.binding.can4home.internal.devices.CANDevice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tinywirepi.GPIO;
import org.tinywirepi.GPIORunCallback;
import org.tinywirepi.SPI;

/**
 * @author William Chèvremont - Initial contribution
 */
@NonNullByDefault
public class MCP2515 extends BaseBridgeHandler implements CAN4HomeProtocol {

    @Nullable
    private SPI m_device;

    private Integer m_intPin = -1;
    private final Logger logger = LoggerFactory.getLogger(MCP2515.class);

    private volatile boolean m_isInitialized = false;
    private volatile boolean m_abordAll = false;
    private volatile boolean m_isSending = false;
    private volatile boolean m_isSendingOk = false;

    public static final byte MCP2515_READRX = (byte) 0x90;
    public static final byte MCP2515_LOADTX = (byte) 0x40;
    public static final byte MCP2515_RTS = (byte) 0x80;
    public static final byte MCP2515_READREG = (byte) 0x03;
    public static final byte MCP2515_WRITEREG = (byte) 0x02;
    public static final byte MCP2515_RESET = (byte) 0xC0;
    public static final byte MCP2515_READST = (byte) 0xA0;
    public static final byte MCP2515_READRXST = (byte) 0xB0;
    public static final byte MCP2515_BITMODIFY = (byte) 0x05;

    @Nullable
    protected GPIORunCallback m_threadCallback;

    HashMap<Integer, CANDevice> m_objects = new HashMap<Integer, CANDevice>();

    public MCP2515(Bridge bridge) {
        super(bridge);
    }

    @Override
    public void handleCommand(ChannelUID channelUID, Command command) {
        // TODO Auto-generated method stub

    }

    @SuppressWarnings("null")
    @Override
    public synchronized void dispose() {
        m_threadCallback.stop();
        m_abordAll = true;
        notifyAll();
        super.dispose();
    }

    @Override
    public synchronized void initialize() {
        m_isInitialized = false;
        m_isSending = false;
        updateStatus(ThingStatus.UNKNOWN);

        logger.debug("Start MCP2515 init...");

        Configuration conf = getConfig();

        logger.debug("After Config");

        Integer speed = Integer.parseInt(conf.get(SPEED).toString());
        m_intPin = Integer.parseInt(conf.get(INTPIN).toString());
        Integer canspeed = 3;

        switch (conf.get(CANSPEED).toString()) {
            case CAN_500kbps:
                canspeed = 0;
                break;
            case CAN_250kbps:
                canspeed = 1;
                break;
            case CAN_125kbps:
                canspeed = 3;
                break;
            case CAN_62p5kbps:
                canspeed = 7;
                break;
        }

        try {
            GPIO.export(m_intPin); // This can be long.

            logger.debug("getInstance");
            m_device = SPI.getInstance(conf.get(DEVICE).toString(), speed, 0);

            // Setup MCP2515
            reset();

            logger.debug("sleep");
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
            }

            logger.debug("config");
            // Set Mode of operation to Config
            bitModify((byte) 0x0F, (byte) 0xE0, (byte) 0x80);

            byte[] cnf = new byte[3];

            // CNF2-3 configure the sample timing (see datasheet)
            cnf[0] = (byte) 0x01; // CNF3
            cnf[1] = (byte) 0xD1; // CNF2
            cnf[2] = (byte) (canspeed & 0x3F); // CNF1 ->Prescaler (this is correct with 8MHz crystal)
            writeRegister((byte) 0x28, cnf);
            writeRegister((byte) 0x2B, (byte) 0x07); // Enable interrupts on message reception (both buffers);
            writeRegister((byte) 0x60, (byte) 0x64); // Turn filters off, rollover RX0-1.

            GPIO.setDirection(m_intPin, true);
            GPIO.setupInterrupt(m_intPin, GPIO.INT_FALLING);

            m_threadCallback = GPIO.pinCallback(m_intPin, () -> this.CanEventCallback(), 3000);

            // Set Mode of operation to Normal
            logger.debug("mode normal");
            bitModify((byte) 0x0F, (byte) 0xE0, (byte) 0x00);

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
            }

            CanEventCallback();
            // I am online !!
            updateStatus(ThingStatus.ONLINE);
            m_isInitialized = true;
            notifyAll();

        } catch (IOException e) {
            updateStatus(ThingStatus.OFFLINE);
            logger.error(e.getMessage());
        }
    }

    @Override
    public void childHandlerInitialized(ThingHandler hand, Thing thg) {

    }

    public void registerThingHandler(CANDevice dev) {
        logger.debug("Import hash: " + String.format("0x%06x", dev.toHash()));
        m_objects.put(dev.toHash(), dev);
    }

    @Override
    public void childHandlerDisposed(ThingHandler hand, Thing thg) {
        if (!(hand instanceof CANDevice)) {
            return;
        }

        CANDevice dev = (CANDevice) hand;
        logger.debug("Unmport hash: " + String.format("0x%06x", dev.toHash()));
        m_objects.remove(dev.toHash());
    }

    public void process(CANPacket pkt) {
        if (pkt.command < 0x70 && !pkt.isRequest) { // Data command
            if (m_objects.containsKey(pkt.toHash())) {
                try {
                    m_objects.get(pkt.toHash()).receive(pkt);
                } catch (PacketException e) {
                    logger.error(e.toString());
                }
            } else {
                logger.debug(String.format("Hash 0x%06x not found!", pkt.toHash()));
            }
        }
    }

    public synchronized void CanEventCallback() {
        try {
            // logger.debug("GPIO: " + GPIO.digitalRead(m_intPin));

            while (!GPIO.digitalRead(m_intPin)) {
                byte st = readStatus();
                logger.debug("Callback " + String.format("0x%02x", st));

                if ((st & 0x01) > 0) { // Buffer 0
                    logger.debug("Message in buffer 0");
                    CANPacket pkt = readRxBuffer(0); // This will clear interrupt
                    logger.debug(pkt.toString());
                    process(pkt);
                }

                if ((st & 0x02) > 0) { // Buffer 1
                    logger.debug("Message in buffer 1");
                    CANPacket pkt = readRxBuffer(1); // This will clear interrupt
                    logger.debug(pkt.toString());
                    process(pkt);
                }

                if ((st & 0x08) > 0) { // Tx0
                    logger.debug("Tx interrupt flag");
                    m_isSendingOk = true;
                    bitModify((byte) 0x2C, (byte) 0x04, (byte) 0x00); // Clear interrupt
                    notifyAll();
                }

                try {
                    Thread.sleep(30); // Wait: maybe another event has to be treated Don't really care of delay: this is
                                      // background!
                } catch (InterruptedException e) {
                }
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    @SuppressWarnings("null")
    protected CANPacket readRxBuffer(int bufno) throws IOException {
        if (bufno < 0 || bufno > 1) {
            throw new IOException("Buffer number is out of range [0,1]");
        }

        byte[] tmp = new byte[14];

        tmp[0] = (byte) (MCP2515_READRX | bufno << 2); // Quick command for RX buffer read (starting from sidh)

        tmp = m_device.transfer(tmp);

        CANPacket r = new CANPacket();

        r.nodeAddress = tmp[1];
        r.groupAddress = (byte) ((tmp[2] & 0xE0) >> 3 | (tmp[2] & 0x3));
        r.command = tmp[3];
        r.cmdOpt = tmp[4];
        r.isRequest = (tmp[5] & 0x40) > 0;
        r.dataLength = (byte) (tmp[5] & 0x0F);

        if (r.dataLength > 8) {
            r.dataLength = 8;
        }

        if (!r.isRequest) {
            r.data = new byte[r.dataLength];
        }

        for (int i = 0; i < r.dataLength; i++) {
            r.data[i] = tmp[i + 6];
        }
        return r;
    }

    @Override
    public void send(CANPacket p) throws IOException {
        send(p, (byte) 0, 10000);
    }

    @SuppressWarnings("null")
    protected synchronized void send(CANPacket p, byte bufno, int timeout) throws IOException {
        if (bufno < 0 || bufno > 2) {
            throw new IOException("Buffer out of range [0,1,2]");
        }

        // Wait if already sending or not already initialized
        while (m_isSending || !m_isInitialized) {

            logger.debug("Attemp to send packet: " + p.toString());
            logger.debug("Already sending: " + m_isSending);
            logger.debug("Is Initialized: " + m_isInitialized);
            logger.debug("Buffno: " + bufno);

            try {
                wait();
            } catch (InterruptedException e) {
            }

            if (m_abordAll) {
                notify();
                return;
            }
        }
        m_isSending = true;

        // Build packet
        byte[] tmp = new byte[14];

        tmp[0] = (byte) (MCP2515_LOADTX | (bufno << 1));
        tmp[1] = p.nodeAddress;
        tmp[2] = (byte) (((p.groupAddress & 0x1C) << 3) | (p.groupAddress & 0x3) | 0x08);
        tmp[3] = p.command;
        tmp[4] = p.cmdOpt;

        if (p.dataLength > 8) {
            p.dataLength = 8;
        }

        if (p.dataLength < 0) {
            p.dataLength = 0;
        }

        tmp[5] = p.dataLength;
        if (p.isRequest) {
            tmp[5] |= 0x40;
        }

        // Fill data if not a RTR
        if (!p.isRequest) {
            for (int i = 0; i < p.dataLength; i++) {
                tmp[6 + i] = p.data[i];
            }
        }

        // Fill buffer
        m_device.transfer(tmp);
        logger.debug("Will send " + p.toString());

        // Delay RTS
        try {
            Thread.sleep(10);
        } catch (InterruptedException e1) {
        }

        // Request to Send
        m_device.transfer((byte) (MCP2515_RTS | (1 << bufno)));
        // byte T = readRegister((byte) 0x30);
        // logger.debug("TXBnCTRL: " + String.format("0x%02x", T));

        m_isSendingOk = false;

        try {
            wait(timeout);
        } catch (InterruptedException e) {
        }

        m_isSending = false;

        if (!m_isSendingOk) {
            byte st = readStatus();
            if ((st & (0x04 << (2 * bufno))) > 0) { // if still sending: abord and throw exception
                bitModify((byte) (0x30 + 0x10 * bufno), (byte) 0x80, (byte) 0x00);
                notify(); // Do not forget to notify!
                throw new IOException("Error while sending the message " + p);
            }
        }

        logger.debug("Message has been sent: " + p.toString());
        notify(); // Unlock next send
    }

    @SuppressWarnings("null")
    protected void writeRegister(byte address, byte[] data) throws IOException {
        byte[] tmp = new byte[2 + data.length];

        tmp[0] = MCP2515_WRITEREG;
        tmp[1] = address;
        for (int i = 0; i < data.length; i++) {
            tmp[i + 2] = data[i];
        }

        m_device.transfer(tmp);
    }

    @SuppressWarnings("null")
    protected void writeRegister(byte address, byte data) throws IOException {
        byte[] tmp = new byte[3];

        tmp[0] = MCP2515_WRITEREG;
        tmp[1] = address;
        tmp[2] = data;

        m_device.transfer(tmp);
    }

    @SuppressWarnings("null")
    protected byte[] readRegister(byte address, byte length) throws IOException {
        byte[] tmp = new byte[2 + length];
        tmp[0] = MCP2515_READREG;
        tmp[1] = address;

        tmp = m_device.transfer(tmp);

        byte[] ret = new byte[length];
        for (int i = 0; i < length; i++) {
            ret[i] = tmp[i + 2];
        }
        return ret;
    }

    @SuppressWarnings("null")
    protected byte readRegister(byte address) throws IOException {
        byte[] tmp = new byte[3];
        tmp[0] = MCP2515_READREG;
        tmp[1] = address;

        tmp = m_device.transfer(tmp);

        return tmp[2];
    }

    @SuppressWarnings("null")
    protected void reset() throws IOException {
        m_device.transfer(MCP2515_RESET);
    }

    @SuppressWarnings("null")
    protected void bitModify(byte address, byte mask, byte value) throws IOException {
        byte[] tmp = new byte[4];
        tmp[0] = MCP2515_BITMODIFY;
        tmp[1] = address;
        tmp[2] = mask;
        tmp[3] = value;

        m_device.transfer(tmp);
    }

    @SuppressWarnings("null")
    protected byte readRxStatus() throws IOException {
        byte[] tmp = new byte[2];
        tmp[0] = MCP2515_READRXST;
        tmp = m_device.transfer(tmp);
        return tmp[1];
    }

    @SuppressWarnings("null")
    protected byte readStatus() throws IOException {
        byte[] tmp = new byte[2];
        tmp[0] = MCP2515_READST;
        tmp = m_device.transfer(tmp);
        return tmp[1];
    }
}
