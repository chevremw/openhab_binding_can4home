/**
 * Copyright (c) 2010-2019 Contributors to the openHAB project
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.openhab.binding.can4home.internal.factory;

import static org.openhab.binding.can4home.internal.CAN4HomeConstants.*;

import java.util.Arrays;
import java.util.Collection;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.smarthome.core.thing.Bridge;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingTypeUID;
import org.eclipse.smarthome.core.thing.binding.BaseThingHandlerFactory;
import org.eclipse.smarthome.core.thing.binding.ThingHandler;
import org.eclipse.smarthome.core.thing.binding.ThingHandlerFactory;
import org.openhab.binding.can4home.internal.bridges.MCP2515;
import org.openhab.binding.can4home.internal.devices.DeviceColorHandler;
import org.openhab.binding.can4home.internal.devices.DeviceContactHandler;
import org.openhab.binding.can4home.internal.devices.DeviceDimmerHandler;
import org.openhab.binding.can4home.internal.devices.DeviceRawValueHandler;
import org.openhab.binding.can4home.internal.devices.DeviceRelHumidityHandler;
import org.openhab.binding.can4home.internal.devices.DeviceRollerHandler;
import org.openhab.binding.can4home.internal.devices.DeviceSwitchHandler;
import org.openhab.binding.can4home.internal.devices.DeviceTemperatureHandler;
import org.osgi.service.component.annotations.Component;

/**
 * @author William Chèvremont - Initial contribution
 */
@NonNullByDefault
@Component(configurationPid = "binding.can4home", service = ThingHandlerFactory.class)
public class CAN4HomeHandlerFactory extends BaseThingHandlerFactory {

    public static final Collection<ThingTypeUID> SUPPORTED_THING_TYPES_UIDS = Arrays.asList(
            THING_CAN4HOME_MCP2515_BRIDGE, THING_CAN4HOME_CONTACT, THING_CAN4HOME_SWITCH, THING_CAN4HOME_COLOR,
            THING_CAN4HOME_DIMMER, THING_CAN4HOME_TEMPERATURE, THING_CAN4HOME_RELHUMID, THING_CAN4HOME_ROLLER,
            THING_CAN4HOME_RAW);

    @Override
    public boolean supportsThingType(ThingTypeUID thingTypeUID) {
        return SUPPORTED_THING_TYPES_UIDS.contains(thingTypeUID);
    }

    @Override
    protected @Nullable ThingHandler createHandler(Thing thing) {
        // TODO Auto-generated method stub
        if (thing.getThingTypeUID().equals(THING_CAN4HOME_MCP2515_BRIDGE)) {
            return new MCP2515((Bridge) thing);
        } else if (thing.getThingTypeUID().equals(THING_CAN4HOME_CONTACT)) {
            return new DeviceContactHandler(thing);
        } else if (thing.getThingTypeUID().equals(THING_CAN4HOME_SWITCH)) {
            return new DeviceSwitchHandler(thing);
        } else if (thing.getThingTypeUID().equals(THING_CAN4HOME_COLOR)) {
            return new DeviceColorHandler(thing);
        } else if (thing.getThingTypeUID().equals(THING_CAN4HOME_DIMMER)) {
            return new DeviceDimmerHandler(thing);
        } else if (thing.getThingTypeUID().equals(THING_CAN4HOME_TEMPERATURE)) {
            return new DeviceTemperatureHandler(thing);
        } else if (thing.getThingTypeUID().equals(THING_CAN4HOME_RELHUMID)) {
            return new DeviceRelHumidityHandler(thing);
        } else if (thing.getThingTypeUID().equals(THING_CAN4HOME_ROLLER)) {
            return new DeviceRollerHandler(thing);
        } else if (thing.getThingTypeUID().equals(THING_CAN4HOME_RAW)) {
            return new DeviceRawValueHandler(thing);
        }
        return null;
    }
}
