/**
 * Copyright (c) 2010-2019 Contributors to the openHAB project
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */

package org.openhab.binding.can4home.internal.devices;

import static org.openhab.binding.can4home.internal.CAN4HomeConstants.*;

import java.io.IOException;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.smarthome.config.core.Configuration;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.binding.BaseThingHandler;
import org.openhab.binding.can4home.internal.bridges.CAN4HomeProtocol.CANPacket;
import org.openhab.binding.can4home.internal.bridges.CAN4HomeProtocol.PacketException;
import org.openhab.binding.can4home.internal.bridges.MCP2515;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author William Chèvremont - Initial contribution
 */
public abstract class CANDevice extends BaseThingHandler {
    private final Logger logger = LoggerFactory.getLogger(CANDevice.class);

    protected byte m_groupAddress = (byte) 0;
    protected byte m_nodeAddress = (byte) 0; // 0 is reserved. So, it says "Hey, I'm unitialized!"
    protected byte m_chanId = (byte) 0;

    protected CANPacket getDataPacket(int n) throws PacketException {
        if (n < 0 || n > 8) {
            throw new PacketException("Length must be between 0 and 8");
        }

        CANPacket pkt = new CANPacket();
        pkt.command = CANPacket.CMD_DATA;
        pkt.cmdOpt = m_chanId;
        pkt.isRequest = false;
        pkt.nodeAddress = m_nodeAddress;
        pkt.groupAddress = m_groupAddress;
        pkt.dataLength = (byte) n;
        pkt.data = new byte[n];

        return pkt;
    }

    protected CANPacket getRequestPacket(int n) throws PacketException {
        if (n < 0 || n > 8) {
            throw new PacketException("Length must be between 0 and 8");
        }

        CANPacket pkt = new CANPacket();
        pkt.command = CANPacket.CMD_DATA;
        pkt.cmdOpt = m_chanId;
        pkt.isRequest = true;
        pkt.nodeAddress = m_nodeAddress;
        pkt.groupAddress = m_groupAddress;
        pkt.dataLength = (byte) n;

        return pkt;
    }

    public CANDevice(Thing thing) {
        super(thing);
    }

    public byte getGroupAddress() {
        return m_groupAddress;
    }

    public byte getNodeAddress() {
        return m_nodeAddress;
    }

    public byte getCANChanId() {
        return m_chanId;
    }

    public Integer toHash() {
        Integer hash = (int) m_chanId;
        hash += m_nodeAddress << 8;
        hash += m_groupAddress << 16;
        return hash;
    }

    protected void checkPacket(CANPacket p) throws PacketException {
        if (!(p.groupAddress == m_groupAddress && p.nodeAddress == m_nodeAddress && p.cmdOpt == m_chanId)) {
            throw new PacketException("This is not my packet!");
        }
    }

    @SuppressWarnings("null")
    @Override
    public void initialize() {
        Configuration conf = getConfig();

        m_groupAddress = (byte) Integer.parseInt(conf.get(GROUP_ADDRESS).toString());
        m_nodeAddress = (byte) Integer.parseInt(conf.get(NODE_ADDRESS).toString());
        m_chanId = (byte) Integer.parseInt(conf.get(CHAN_NUMBER).toString());

        MCP2515 bdg = (MCP2515) getBridge().getHandler();
        bdg.registerThingHandler(this);

        new Thread(() -> {
            CANPacket pkt = new CANPacket();
            pkt.command = CANPacket.CMD_CHANINFO;
            pkt.cmdOpt = m_chanId;
            pkt.groupAddress = m_groupAddress;
            pkt.nodeAddress = m_nodeAddress;
            pkt.isRequest = true;
            pkt.dataLength = (byte) 1;

            try {
                bdg.send(pkt);
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }).start(); // Send the check request asynchronously
    }

    public abstract void receive(@NonNull CANPacket p) throws PacketException;
}
