/**
 * Copyright (c) 2010-2019 Contributors to the openHAB project
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.openhab.binding.can4home.internal.devices;

import static org.openhab.binding.can4home.internal.CAN4HomeConstants.*;

import java.io.IOException;
import java.math.BigDecimal;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.smarthome.core.library.types.DecimalType;
import org.eclipse.smarthome.core.library.types.QuantityType;
import org.eclipse.smarthome.core.library.unit.SIUnits;
import org.eclipse.smarthome.core.thing.Bridge;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingStatus;
import org.eclipse.smarthome.core.thing.ThingStatusDetail;
import org.eclipse.smarthome.core.types.Command;
import org.eclipse.smarthome.core.types.RefreshType;
import org.openhab.binding.can4home.internal.bridges.CAN4HomeProtocol;
import org.openhab.binding.can4home.internal.bridges.CAN4HomeProtocol.CANPacket;
import org.openhab.binding.can4home.internal.bridges.CAN4HomeProtocol.PacketException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The {@link CAN4HomeHandler} is responsible for handling commands, which are
 * sent to one of the channels.
 *
 * @author William Chèvremont - Initial contribution
 */
@NonNullByDefault
public class DeviceTemperatureHandler extends CANDevice {
    private final Logger logger = LoggerFactory.getLogger(DeviceTemperatureHandler.class);

    public DeviceTemperatureHandler(Thing thing) {
        super(thing);
    }

    // Commands from openHAB
    @SuppressWarnings("null")
    @Override
    public void handleCommand(ChannelUID channelUID, Command command) {
        if (command == RefreshType.REFRESH) { // Emit Request

        } else if (DecimalType.class.isInstance(command)) {
            try {
                Bridge B = getBridge();
                if (B != null && CAN4HomeProtocol.class.isInstance(B.getHandler())) {
                    CAN4HomeProtocol bdg = (CAN4HomeProtocol) B.getHandler();
                    CANPacket pkt = getDataPacket(2);
                    pkt.command = CANPacket.CMD_WRITE;
                    DecimalType tmp = (DecimalType) command;
                    int Tmp = (int) (tmp.doubleValue() * 100. + 27315.);
                    pkt.data[0] = (byte) ((Tmp & 0x0000FF00) >> 8);
                    pkt.data[1] = (byte) (Tmp & 0x000000FF);
                    bdg.send(pkt);
                }
            } catch (PacketException | IOException e) {
                logger.error(e.getMessage());
            }
        }
    }

    // Commands from CAN bus
    @Override
    public void receive(CANPacket p) throws PacketException {
        checkPacket(p);

        if (p.command == CANPacket.CMD_DATA) {
            if (p.data.length != 2) {
                throw new PacketException("Data must be one uint16");
            }

            // Conversion from CAN4Home protocol to Integer then double
            int tmp = 0;
            tmp += (p.data[1]) & 0x000000FF;
            tmp += ((p.data[0] << 8) & 0x0000FF00);
            tmp -= 27315;

            logger.debug("Received temp: " + tmp);

            BigDecimal value = new BigDecimal(tmp);
            updateState("value", new QuantityType<>(value.divide(new BigDecimal(100)), SIUnits.CELSIUS));
        } else if (p.command == CANPacket.CMD_CHANINFO) { // Check if it is the right type
            byte type = p.data[0];
            if ((type & TYPES_MASK) == TYPE_TEMP) {
                updateStatus(ThingStatus.ONLINE);
            } else {
                updateStatus(ThingStatus.OFFLINE, ThingStatusDetail.CONFIGURATION_ERROR);
            }
        } else {
            logger.debug("Unimplemented CAN command " + p.command);
        }
    }

    @Override
    public void initialize() {
        updateStatus(ThingStatus.UNKNOWN);
        super.initialize();
    }
}
